﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CameraFollow : MonoBehaviour {

    public Camera cam;
    public float ClampAngle = 25.0f;
    public float MouseX;
    public float MouseY;
    public float FinalInputX;
    public float FinalInputZ;

    private float MouseSensitivity = 1;
    private float RotY = 0.0f;
    private float RotX = 0.0f;

    void Start () {

        Vector3 rot = transform.localRotation.eulerAngles;
        RotY = rot.y;
        RotX = rot.x;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
	
	void Update () {
        MouseX = Input.GetAxis("Mouse Y") * MouseSensitivity;
        MouseY = Input.GetAxis("Mouse X") * MouseSensitivity;

        RotX -= MouseX;
        RotY += MouseY;

        RotX = Mathf.Clamp(RotX, -ClampAngle, ClampAngle);

        Quaternion localRotation = Quaternion.Euler(RotX, RotY, 0.0f);
        transform.rotation = localRotation;

    }

    public void DisableMouse()
    {
        MouseSensitivity = 0;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void EnableMouse()
    {
        MouseSensitivity = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
}
