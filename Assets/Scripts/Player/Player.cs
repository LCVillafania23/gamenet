﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;

public class Player : NetworkBehaviour {
    public UnityEvent OnGunFire;
    public UnityEvent OnBlueGunFire;
    public UnityEvent OnRedGunFire;
    public UnityEvent OnLivesDeplete;

    [SyncVar]
    public int lives;

    public GameObject BulletPrefab;
    public Transform BulletSpawn;
    public float BulletSpeed;

    private ColorCode color;
    private Crosshair crosshair;

	void Start () {
        color = GetComponent<ColorCode>();
        crosshair = GetComponent<Crosshair>();

        if (GetComponent<NetworkIdentity>().isServer)
            color.UnitColor = ColorCode.ColorID.Blue;
        else
            color.UnitColor = ColorCode.ColorID.Red;
        Debug.Log(color.UnitColor);
    }

    // Update is called once per frame
    void Update () {
        //if (!isLocalPlayer)
        //    return;

        if (Input.GetButtonDown("Fire1"))
        {
            CmdFire();
        }
    }

    [Command]
    void CmdFire()
    {
        var bullet = (GameObject)Instantiate(
            BulletPrefab,
            BulletSpawn.position,
            BulletSpawn.rotation);

        Vector3 direction = new Vector3();

        //if (crosshair.GetCrosshairHit() != Vector3.zero)
        //{
        //    direction = (crosshair.GetCrosshairHit() - BulletSpawn.position).normalized;
        //}
        //else
        direction = crosshair.cam.transform.forward;

        NetworkServer.Spawn(bullet);

        bullet.GetComponent<ColorCode>().ChangeColor(GetComponent<ColorCode>().UnitColor);
        bullet.GetComponent<Rigidbody>().velocity = (bullet.transform.forward * BulletSpeed);
   
        if (color.UnitColor == ColorCode.ColorID.Blue)
            OnBlueGunFire.Invoke();
        else OnRedGunFire.Invoke();

        OnGunFire.Invoke();

        Destroy(bullet, 2.0f);
    }

    public void DecreaseLife() {
        lives--;

        if (lives <= 0)
            OnLivesDeplete.Invoke();
    }
}
