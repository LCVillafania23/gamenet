﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerMovement : NetworkBehaviour {

    public Camera cam;

    public float Speed;
    public float RotateSpeed;
    public Vector3 MoveDirection = Vector3.zero;

    private Rigidbody rb;
    private Animator animator;

	void Start () {
        if (!isLocalPlayer)
        {
            cam.enabled = false;
            return;
        }
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        Vector3 xMovement = new Vector3(cam.transform.right.x, 0, cam.transform.right.z) * Input.GetAxis("Horizontal");
        Vector3 zMovement = new Vector3(cam.transform.forward.x, 0, cam.transform.forward.z) * Input.GetAxis("Vertical");

        MoveDirection = (xMovement + zMovement).normalized * Speed;

        Quaternion localRotation = Quaternion.Euler(transform.rotation.x, cam.gameObject.transform.eulerAngles.y, transform.rotation.z);
        transform.rotation = localRotation;
    }

    private void FixedUpdate()
    {
        if (!isLocalPlayer)
            return;
        rb.MovePosition(rb.position + MoveDirection * Time.fixedDeltaTime);
    }

    public void IsDead()
    {
        animator.SetTrigger("isDead");
    }

    public void HasWon()
    {
        animator.SetTrigger("HasWon");
    }
}
