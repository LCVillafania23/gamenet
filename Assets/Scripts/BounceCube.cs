﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BounceCube : MonoBehaviour {

    public UnityEvent OnPlaneBounce;
    public UnityEvent BlueBounce;
    public UnityEvent RedBounce;

    public float Force;

    private Animator animator;
    private ColorCode color;
    private void Start()
    {
        color = GetComponent<ColorCode>();
        animator = GetComponent<Animator>();
        if (color.UnitColor == ColorCode.ColorID.Blue)
            animator.SetTrigger("isBlue");
        else if (color.UnitColor == ColorCode.ColorID.Red)
            animator.SetTrigger("isRed");
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player") {
            Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
            ColorCode c = other.gameObject.GetComponent<ColorCode>();

            rb.AddForce(Force * Vector3.up, ForceMode.Impulse);
            OnPlaneBounce.Invoke();

            if (color.UnitColor == ColorCode.ColorID.Blue)
                BlueBounce.Invoke();
            else
                RedBounce.Invoke();

            if (rb != null && c.UnitColor != color.UnitColor)
            {
                other.gameObject.GetComponent<Player>().DecreaseLife();
            }

            else return;
        }

        if (other.gameObject.tag == "Bullet")
        {
            color.ChangeColor(color.UnitColor);
            //Change Color
            if (color.UnitColor == ColorCode.ColorID.Blue)
                animator.SetTrigger("isBlue");
            else if (color.UnitColor == ColorCode.ColorID.Red)
                animator.SetTrigger("isRed");
        }
    }

    public void ChangePlaneColor(int c) {
        switch (c) {
            case 0:
                animator.SetTrigger("isBlue");
                break;
            case 1:
                animator.SetTrigger("isRed");
                break;
        }
    }
}
