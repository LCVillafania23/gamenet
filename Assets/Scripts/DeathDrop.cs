﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DeathDrop : MonoBehaviour {
    public UnityEvent OnBlueDrop;
    public UnityEvent OnRedDrop;

    private void OnTriggerEnter(Collider other)
    {
        ColorCode cc = other.gameObject.GetComponent<ColorCode>();
        if(cc != null && other.gameObject.tag == "Player")
        {
            if (cc.UnitColor == ColorCode.ColorID.Blue)
                OnBlueDrop.Invoke();
            if (cc.UnitColor == ColorCode.ColorID.Red)
                OnRedDrop.Invoke();
        }
    }
}
