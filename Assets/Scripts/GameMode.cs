﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameMode : MonoBehaviour {
    public UnityEvent OnBlueWin;
    public UnityEvent OnRedWin;

    public List<ColorCode> BouncyColors;

    public int BlueBouncyCount { get; private set; }
    public int RedBouncyCount { get; private set; }

    void Start () {
        CheckBouncyColorCount();
    }

    public void CheckBouncyColorCount() {
        int blue = 0;
        int red = 0;

        for (int i = 0; i < BouncyColors.Count; i++) {
            if (BouncyColors[i].UnitColor == ColorCode.ColorID.Blue)
                blue++;
            if (BouncyColors[i].UnitColor == ColorCode.ColorID.Red)
                red++;
        }

        BlueBouncyCount = blue;
        RedBouncyCount = red;
    }

    public void CheckForWinner() {
        CheckBouncyColorCount();

        if (BlueBouncyCount > RedBouncyCount)
            OnBlueWin.Invoke();
        else
            OnRedWin.Invoke();
    }
}
