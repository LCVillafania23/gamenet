﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class Bullet : MonoBehaviour {
    public UnityEvent OnEnemyHit;
    public ParticleSystem BlueHit;
    public ParticleSystem RedHit;
    public float Force;

    private Rigidbody rb;
    private ColorCode colorCode;
    private ParticleSystem BlueHitParticle;
    private ParticleSystem RedHitParticle;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        colorCode = GetComponent<ColorCode>();
	}

    private void OnCollisionEnter(Collision collision)
    {
        GameObject other = collision.gameObject;
        Rigidbody otherRB = other.GetComponent<Rigidbody>();
        ColorCode cc = other.GetComponent<ColorCode>();
        PlayParticleHit();

        if (other.tag == "Player" && cc.UnitColor != colorCode.UnitColor) {
            ForcePush(otherRB);
            OnEnemyHit.Invoke();
            PlayParticleHit();
        }

        if (other.tag == "BouncyBoi" && cc.UnitColor != colorCode.UnitColor) {
            cc.ChangeColor(colorCode.UnitColor);
        }

        Destroy(gameObject);
    }

    private void ForcePush(Rigidbody rigidbody) {
        rigidbody.AddForce(rb.velocity.normalized * Force);
    }

    private void PlayParticleHit() {
        if (colorCode.UnitColor == ColorCode.ColorID.Blue)
        {
            BlueHitParticle = Instantiate(BlueHit, transform.position, Quaternion.identity);
            BlueHitParticle.Play();
        }
        else
        {
            RedHitParticle = Instantiate(RedHit, transform.position, Quaternion.identity);
            RedHitParticle.Play();
        }
    }
}
