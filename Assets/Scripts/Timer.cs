﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Timer : NetworkBehaviour {
    public int InitialTime;
    public GameMode gameMode;
    public Text time;

    [SyncVar]
    private int currentTime;

    private void Start()
    {
        currentTime = InitialTime;
        StartCoroutine(Begin());
    }

    private void FixedUpdate()
    {
        time.text = "Time: " + currentTime.ToString() + "s";
    }

    IEnumerator Begin()
    {
        while (currentTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            currentTime--;
        }

        gameMode.CheckForWinner();
    }
}
