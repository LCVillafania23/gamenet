﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class GameManagerScript : NetworkBehaviour {
    public GameObject Player_Blue;
    public GameObject Player_Red;
    public GameObject BluePanel;
    public GameObject RedPanel;

    private void Start()
    {
        //Player_Blue = NetworkManager.singleton.client.connection.playerControllers[0].gameObject;
        //Player_Red = NetworkManager.singleton.client.connection.playerControllers[1].gameObject;
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void BlueWon()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        StartCoroutine(Blue());
    }

    public void RedWon()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        StartCoroutine(Red());
    }

    IEnumerator Blue()
    {
        //Player_Blue.GetComponent<PlayerMovement>().HasWon();
        yield return new WaitForSeconds(2.0f);
        BluePanel.SetActive(true);
    }

    IEnumerator Red()
    {
        //Player_Red.GetComponent<PlayerMovement>().HasWon();
        yield return new WaitForSeconds(2.0f);
        RedPanel.SetActive(true);
    }
}
