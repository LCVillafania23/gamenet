﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour {

    public Camera cam;

    private void Update()
    {
        Debug.DrawRay(cam.transform.position, cam.transform.forward, Color.green);
    }

    public Vector3 GetCrosshairHit()
    {
        RaycastHit hit;
        //Debug.DrawRay(cam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0)), cam.transform.forward, Color.green,100);
        if (Physics.Raycast(cam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0)), cam.transform.forward, out hit, 100))
        {
            return hit.point;
        }
        else
            return Vector3.zero;
    }
}
