﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkHandler : NetworkManager {
    public GameManagerScript Manager;

    public override void OnStopClient()
    {
        Cursor.visible = (true);
        base.OnStopClient();
    }

    public override void OnStopHost()
    {
        Cursor.visible = (true);
        base.OnStopHost();
    }

    public override void OnStartHost()
    {
        Cursor.visible = (false);
        base.OnStartHost();
    }

    public override void OnStartClient(NetworkClient client)
    {
        Manager.enabled = true;
        Cursor.visible = (false);
        base.OnStartClient(client);
    }
}