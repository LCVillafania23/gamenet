﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class LivesUI : NetworkBehaviour {
    public Text Lives;
    public GameManagerScript GM;

    private GameObject player;

    private void Start()
    {
        if (isServer)
            player = GM.Player_Blue;
        else if (isClient)
            player = GM.Player_Red;
    }

    private void FixedUpdate()
    {
        Lives.text = "Lives: " + player.GetComponent<Player>().lives.ToString();
    }
}
