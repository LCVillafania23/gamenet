﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncePlane : MonoBehaviour {

    public float Force;

    private Animator animator;
    private ColorCode color;
    private void Start()
    {
        color = GetComponent<ColorCode>();
        animator = GetComponent<Animator>();
        if (color.UnitColor == ColorCode.ColorID.Blue)
            animator.SetTrigger("isBlue");
        else if (color.UnitColor == ColorCode.ColorID.Red)
            animator.SetTrigger("isRed");
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player") {
            Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
            ColorCode c = other.gameObject.GetComponent<ColorCode>();

            if (rb != null && c.UnitColor == color.UnitColor)
            {
                rb.AddForce(Force * Vector3.up, ForceMode.Impulse);
            }
            else return;
        }

        if (other.gameObject.tag == "Bullet")
        {
            color.ChangeColor(color.UnitColor);
            //Change Color
            if (color.UnitColor == ColorCode.ColorID.Blue)
                animator.SetTrigger("isBlue");
            else if (color.UnitColor == ColorCode.ColorID.Red)
                animator.SetTrigger("isRed");
        }
    }
}
