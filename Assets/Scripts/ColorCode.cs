﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColorCode : MonoBehaviour {

    public UnityEvent OnChangeToBlue;
    public UnityEvent OnChangeToRed;

    public enum ColorID { Blue,Red };

    public ColorID UnitColor;

    public void ChangeColor(ColorID color) {

        switch (color) {
            case ColorID.Blue:
                if (UnitColor != ColorID.Blue) {
                    UnitColor = ColorID.Blue;
                    // Change to Blue code here
                    OnChangeToBlue.Invoke();
                }
                break;

            case ColorID.Red:
                if (UnitColor != ColorID.Red)
                {
                    UnitColor = ColorID.Red;
                    // Change to Red code here
                    OnChangeToRed.Invoke();
                }
                break;
        }

    }
}
